From f541da70a5fdb553dc0cb71b3de54065dd245c2d Mon Sep 17 00:00:00 2001
From: jiangheng <jiangheng14@huawei.com>
Date: Mon, 19 Jun 2023 00:56:38 +0800
Subject: [PATCH] gro:fix gro with tcp push flag

TCP data packets sometimes carry a PUSH flag. Currently,
only the packets that do not have PUSH flag can be GROed.
The packets that have a PUSH flag cannot be GROed, the packets
that cannot be processed by GRO are placed last.
In this case, the received packets may be out of order.
For example, there are two packets mbuf1 and mbuf2. mbuf1
contains PUSH flag, mbuf2 does not contain PUSH flag.
After GRO processing, mbuf2 is sent for processing before mbuf1.
This out-of-order will affect TCP processing performance and
lead to unnecessary dup-ACK.

Referring to the Linux kernel implementation, packets with PUSH
flag can also perform GRO. And if one of the packets containing
PUSH flag, the packets after GRO will carry PUSH flag.

Reference: https://mails.dpdk.org/archives/stable/2022-July/039759.html
---
 lib/gro/gro_tcp.h  | 16 +++++++++++++---
 lib/gro/gro_tcp4.c |  3 ++-
 2 files changed, 15 insertions(+), 4 deletions(-)

diff --git a/lib/gro/gro_tcp.h b/lib/gro/gro_tcp.h
index d926c4b..5f96ea3 100644
--- a/lib/gro/gro_tcp.h
+++ b/lib/gro/gro_tcp.h
@@ -85,7 +85,8 @@ merge_two_tcp_packets(struct gro_tcp_item *item,
 		uint16_t l2_offset)
 {
 	struct rte_mbuf *pkt_head, *pkt_tail, *lastseg;
-	uint16_t hdr_len, l2_len;
+	struct rte_tcp_hdr *head_tcp_hdr, *tail_tcp_hdr;
+	uint16_t hdr_len, l2_len, l3_offset;
 
 	if (cmp > 0) {
 		pkt_head = item->firstseg;
@@ -96,8 +97,8 @@ merge_two_tcp_packets(struct gro_tcp_item *item,
 	}
 
 	/* check if the IPv4 packet length is greater than the max value */
-	hdr_len = l2_offset + pkt_head->l2_len + pkt_head->l3_len +
-		pkt_head->l4_len;
+	l3_offset = l2_offset + pkt_head->l2_len + pkt_head->l3_len;
+	hdr_len = l3_offset + pkt_head->l4_len;
 	l2_len = l2_offset > 0 ? pkt_head->outer_l2_len : pkt_head->l2_len;
 	if (unlikely(pkt_head->pkt_len - l2_len + pkt_tail->pkt_len -
 				hdr_len > MAX_IP_PKT_LENGTH))
@@ -106,6 +107,15 @@ merge_two_tcp_packets(struct gro_tcp_item *item,
 	if (unlikely(pkt_head->nb_segs >= 20))
 		return 0;
 
+	/* merge push flag to pkt_head */
+	tail_tcp_hdr = rte_pktmbuf_mtod_offset(pkt_tail,
+		struct rte_tcp_hdr *, l3_offset);
+	if (tail_tcp_hdr->tcp_flags & RTE_TCP_PSH_FLAG) {
+		head_tcp_hdr = rte_pktmbuf_mtod_offset(pkt_head,
+			struct rte_tcp_hdr *, l3_offset);
+		head_tcp_hdr->tcp_flags |= RTE_TCP_PSH_FLAG;
+	}
+
 	/* remove the packet header for the tail packet */
 	rte_pktmbuf_adj(pkt_tail, hdr_len);
 
diff --git a/lib/gro/gro_tcp4.c b/lib/gro/gro_tcp4.c
index 6645de5..f771e81 100644
--- a/lib/gro/gro_tcp4.c
+++ b/lib/gro/gro_tcp4.c
@@ -140,10 +140,11 @@ gro_tcp4_reassemble(struct rte_mbuf *pkt,
 	hdr_len = pkt->l2_len + pkt->l3_len + pkt->l4_len;
 
 	/*
-	 * Don't process the packet which has FIN, SYN, RST, PSH, URG, ECE
+	 * Don't process the packet which has FIN, SYN, RST, URG, ECE
 	 * or CWR set.
 	 */
 	if (tcp_hdr->tcp_flags != RTE_TCP_ACK_FLAG)
+	if (tcp_hdr->tcp_flags & (~(RTE_TCP_ACK_FLAG | RTE_TCP_PSH_FLAG)))
 		return -1;
 
 	/* trim the tail padding bytes */
-- 
2.33.0

